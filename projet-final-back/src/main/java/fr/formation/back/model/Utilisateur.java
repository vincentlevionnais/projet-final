package fr.formation.back.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Utilisateur {
	
	@Id
	private String identifiant;
	private String password;
	@OneToMany(mappedBy="utilisateur", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnoreProperties("utilisateur")
	private List<RoleUtilisateur> roles;
	
	@OneToOne
	@JoinColumn(name= "personne_id")
	private Personne personne;
	

	public Utilisateur() {
	}
	
	public Utilisateur(String identifiant, String password, List<RoleUtilisateur> roles) {
		this.setIdentifiant(identifiant); 
		this.setPassword(password);
		this.roles = roles;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;		
	}
	
	
	    
	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	@Override
	public String toString() {
		return "Utilisateur [identifiant=" + identifiant + ", password=" + password + ", roles=" + roles + "]";
	}

	public List<RoleUtilisateur> getRoles() {
		return roles;
	}
	
	@JsonIgnore
	@Transient
	public List<String> getStringRoles() {
		 List<String> listRoles = new ArrayList<String>();
		 for (RoleUtilisateur roleUtilisateur : roles) {
			listRoles.add(roleUtilisateur.getRole().toString());
		}
		 return listRoles;
		 
// équivalent à :
//	  List<String> result = getroles.stream().map((RoleUtilisateur role) -> {
//		  return role.toString();
//		  })
//		.collect(Collectors.toList());
//	  	return result;
		
	}
	
	public void setRoles(List<RoleUtilisateur> roles) {
		this.roles = roles;
	}

//	@JsonIgnore
//	public Collection<? extends GrantedAuthority> getRolesAsAuthorities() {
//		return this.roles.stream().map(r -> new SimpleGrantedAuthority(r.getRole().name()))
//				.collect(Collectors.toList());
//	}
	
	public UserDetails toUserDetails() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for(RoleUtilisateur role : roles) {
			GrantedAuthority ga = new SimpleGrantedAuthority(role.getRole().name());
			authorities.add(ga);
		}
		return new User(this.identifiant, this.password,authorities);
	}

	
}
