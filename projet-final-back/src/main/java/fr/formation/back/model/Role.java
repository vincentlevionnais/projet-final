package fr.formation.back.model;

public enum Role {
	ROLE_GESTIONNAIRE,ROLE_UTILISATEUR,ROLE_STAGIAIRE,ROLE_FORMATEUR;
	
	public String toString() {
		
		switch(this) {
		case ROLE_GESTIONNAIRE :
			return "ROLE_GESTIONNAIRE";
		case ROLE_UTILISATEUR :
			return "ROLE_UTILISATEUR";
		case ROLE_STAGIAIRE :
			return "ROLE_STAGIAIRE";
		case ROLE_FORMATEUR :
			return "ROLE_FORMATEUR";
		}
		
		return null;
	}
}

