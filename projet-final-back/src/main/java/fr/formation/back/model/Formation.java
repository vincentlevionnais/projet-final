package fr.formation.back.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Formation {

	@Id
	private String libelle;

	@OneToMany(mappedBy = "formation")
	@JsonIgnoreProperties("formation")
	private List<Filiere> filieres;

	@OneToMany(mappedBy = "formation")
	@JsonIgnoreProperties({ "filiere", "formation" })
	private List<Personne> stagiaires;

	// Constructeurs
	public Formation() {

	}

	public Formation(String libelle, List<Filiere> filieres, List<Personne> stagiaires) {
		super();
		this.libelle = libelle;
		this.filieres = filieres;
		this.stagiaires = stagiaires;
	}

	// Getters & setters

	public List<Filiere> getFilieres() {
		return filieres;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public void setFilieres(List<Filiere> filieres) {
		this.filieres = filieres;
	}

	public List<Personne> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Personne> stagiaires) {
		this.stagiaires = stagiaires;
	}

}
