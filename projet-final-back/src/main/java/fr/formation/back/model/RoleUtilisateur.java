package fr.formation.back.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class RoleUtilisateur {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="utilisateur_identifiant")
	@JsonIgnoreProperties("roles")
	private Utilisateur utilisateur;
	@Enumerated(EnumType.STRING)
	private Role role;
	
	public RoleUtilisateur() {
		super();
	}

	public RoleUtilisateur(Integer id, Utilisateur utilisateur, Role role) {
		super();
		this.id = id;
		this.utilisateur = utilisateur;
		this.role = role;
	}

	public RoleUtilisateur(Utilisateur utilisateur, Role role) {
		this.utilisateur = utilisateur;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Role getRole() {
		return role;
	}
	
	@JsonIgnore
	@Transient
	public String getStringRole() {
		return role.toString();
	}
	

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "RoleUtilisateur [id=" + id + ", role=" + role + "]";
	}
	
}
