package fr.formation.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.formation.back.model.Utilisateur;

public interface AuthentificationRepository extends JpaRepository<Utilisateur, String> {

    @Query(" select u from Utilisateur u where u.identifiant = ?1")
    Utilisateur findUserById(String identifiant);
}