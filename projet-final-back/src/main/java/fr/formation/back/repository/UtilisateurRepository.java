package fr.formation.back.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.formation.back.model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, String> {

    @Query(" select u from Utilisateur u where u.identifiant = ?1")
    Optional<Utilisateur> findUserById(String identifiant);
}
