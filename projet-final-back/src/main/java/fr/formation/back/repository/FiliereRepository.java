package fr.formation.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.back.model.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Integer> {

}
