package fr.formation.back.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.back.model.Filiere;
import fr.formation.back.model.Module;

public interface ModuleRepository extends JpaRepository<Module, Integer> {

	List<Module> findByFiliere(Filiere f);

	List<Module> findByFormateurIsNull();
}
