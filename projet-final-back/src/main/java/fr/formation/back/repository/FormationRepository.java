package fr.formation.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.back.model.Formation;

public interface FormationRepository extends JpaRepository<Formation, String> {

}
