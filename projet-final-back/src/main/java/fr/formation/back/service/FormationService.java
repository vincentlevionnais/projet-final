package fr.formation.back.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.back.model.Formation;
import fr.formation.back.repository.FormationRepository;

@Service
public class FormationService {
	
	@Autowired
	private FormationRepository fr;
	
	public void create(Formation f) {
		this.fr.save(f);
	}
	
	public List<Formation> findAll() {
		return this.fr.findAll();
	}
	
	public Optional<Formation> getById(String id) {
		return this.fr.findById(id);
	}
	
	public void update(Formation f) {
		this.fr.save(f);
	}
	
	public Optional<Boolean> delete(Formation f) {
		try {
			this.fr.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(String id) {
		try {
			this.fr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

}
