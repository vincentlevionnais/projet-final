package fr.formation.back.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.back.model.Utilisateur;
import fr.formation.back.repository.UtilisateurRepository;

@Service
public class AuthService implements UserDetailsService {

	@Autowired
	UtilisateurRepository ur;
	
	@Override
	public UserDetails loadUserByUsername(String identifiant) throws UsernameNotFoundException {
		Optional<Utilisateur> optU = ur.findUserById(identifiant);
		
			if(optU.isPresent()) {
				return optU.get().toUserDetails();
			} else {
				throw new UsernameNotFoundException("L'utilisateur "+ identifiant +" n'existe pas.");
			}
	}
}
