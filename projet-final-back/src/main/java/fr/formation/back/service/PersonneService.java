package fr.formation.back.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.back.model.Filiere;
import fr.formation.back.model.Personne;
import fr.formation.back.model.Role;
import fr.formation.back.model.RoleUtilisateur;
import fr.formation.back.model.TypePersonne;
import fr.formation.back.model.Utilisateur;
import fr.formation.back.repository.PersonneRepository;
import fr.formation.back.repository.UtilisateurRepository;

@Service
public class PersonneService {

	@Autowired
	private PersonneRepository pr;

	@Autowired
	private UtilisateurRepository ur;

	public void create(Utilisateur u){
		this.ur.save(u);
	}

	public Personne create(Personne p) {
		return this.pr.save(p);
	}

	public List<Personne> findAll() {
		return this.pr.findAll();
	}

	public Optional<Personne> getById(Integer id) {
		return pr.findById(id);
	}

	public void update(Personne p) {
		this.pr.save(p);
	}

	public Optional<Boolean> delete(Personne p) {
		try {
			this.pr.delete(p);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(Integer id) {
		try {
			this.pr.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Personne> getFormateursOfFiliere(Filiere f) {
		return pr.findFormateursOfFiliere(f);
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiairesToFiliere(List<Personne> pList, Filiere f) {
		for (Personne p : pList) {
			pr.bindPersonneToFiliere(p.getId(), f);
		}
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiaireToFiliere(Personne p, Filiere f) {
		pr.bindPersonneToFiliere(p.getId(), f);
	}

	public Boolean checkAllStagiaire(List<Personne> pList) {
		for (Personne p : pList) {
			if (!TypePersonne.STAGIAIRE.equals(pr.getById(p.getId()).getType()))
				return false;
		}
		return true;
	}

	@Transactional
	public void inscription(Personne personne) {
		String identifiant = personne.getPrenom().substring(0,1) + personne.getNom().toLowerCase();
		String mdp = personne.getPrenom() + personne.getNom();
		
		Utilisateur u = new Utilisateur();
		u.setIdentifiant(identifiant);
		u.setPassword(mdp);
		
		//RoleUtilisateur defaultRole = new RoleUtilisateur(u, Role.ROLE_UTILISATEUR);
		RoleUtilisateur defaultRole = new RoleUtilisateur(u, Role.ROLE_GESTIONNAIRE);
        u.setRoles(Arrays.asList(defaultRole));

        Personne p = create(personne);
        u.setPersonne(p);
		create(u);
		
    }
}
