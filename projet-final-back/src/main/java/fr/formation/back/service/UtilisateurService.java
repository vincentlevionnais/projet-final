package fr.formation.back.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.formation.back.model.Role;
import fr.formation.back.model.RoleUtilisateur;
import fr.formation.back.model.Utilisateur;
import fr.formation.back.repository.RoleUtilisateurRepository;
import fr.formation.back.repository.UtilisateurRepository;

@Service
public class UtilisateurService {
	
	@Autowired
	UtilisateurRepository ur;
	
	@Autowired
	RoleUtilisateurRepository rur;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	public Utilisateur create(Utilisateur u) {
		u.setPassword(passwordEncoder.encode(u.getPassword()));
		ur.save(u);
		
		// Roles
		for (RoleUtilisateur roleUtilisateur : u.getRoles()) {
			roleUtilisateur.setUtilisateur(u);
			
			create(roleUtilisateur);
		}
		
		return u;
	}
	
	public RoleUtilisateur create (RoleUtilisateur roleUtilisateur) {
		return rur.findByUtilisateurAndRole(roleUtilisateur.getUtilisateur().getIdentifiant(), roleUtilisateur.getRole())
				.orElseGet(() -> rur.save(roleUtilisateur));
	}

	public List<Utilisateur> findAll() {
		return this.ur.findAll();
	}

	public Optional<Utilisateur> getById(String identifiant) {
		return ur.findById(identifiant);
	}

	public void update(Utilisateur u) {
		this.ur.save(u);
	}

	public Optional<Boolean> delete(Utilisateur u) {
		try {
			this.ur.delete(u);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(String identifiant) {
		try {
			this.ur.deleteById(identifiant);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public void inscription(Utilisateur utilisateur) {
        //RoleUtilisateur defaultRole = new RoleUtilisateur(utilisateur, Role.ROLE_UTILISATEUR);
		RoleUtilisateur defaultRole = new RoleUtilisateur(utilisateur, Role.ROLE_GESTIONNAIRE);

        // OPTIMISATION :
        utilisateur.setRoles(Arrays.asList(defaultRole));

        create(utilisateur);
    }

}
