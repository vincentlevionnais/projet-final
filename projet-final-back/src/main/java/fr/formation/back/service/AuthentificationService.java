package fr.formation.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.formation.back.model.Utilisateur;
import fr.formation.back.repository.AuthentificationRepository;

@Service
public class AuthentificationService {
    
    @Autowired
	private AuthentificationRepository ar;


    public Utilisateur loginCheck(String identifiant, String password) {
        Utilisateur u = null;

        try {
            u = ar.findUserById(identifiant);
            if(password == u.getPassword()) {
            	return u;
            }
        } catch (Exception e) {
           throw e;
        }
        return null;
	}
}
