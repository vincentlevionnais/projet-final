package fr.formation.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.back.model.Utilisateur;
import fr.formation.back.service.AuthentificationService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/authentification")
public class AuthentificationController {
    
    @Autowired
	AuthentificationService as;
	
    @PostMapping("")
    public Utilisateur login(@RequestBody Utilisateur u) {
        as.loginCheck(u.getIdentifiant(), u.getPassword());
        return u;
    }

}
