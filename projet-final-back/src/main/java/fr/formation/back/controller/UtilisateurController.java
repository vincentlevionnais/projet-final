package fr.formation.back.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import fr.formation.back.model.Utilisateur;
import fr.formation.back.service.UtilisateurService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/utilisateur")
public class UtilisateurController {

	@Autowired
	private UtilisateurService us;

	@GetMapping("")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Utilisateur> findAll() {
		return us.findAll();
	}

	@GetMapping("/{identifiant}")
	public Utilisateur getById(@PathVariable String identifiant) {
		return us.getById(identifiant).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur "+ identifiant +" n'existe pas"));
	}

	@PostMapping("")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@ResponseStatus(HttpStatus.CREATED)
	public void createUtilisateur(@RequestBody Utilisateur u) {
		us.create(u);
	}

	
	@PutMapping("")
	public void updateUtilisateur(@RequestBody Utilisateur u) {
		us.update(u);
	}

	@PutMapping("/{identifiant}")
	public void deleteUtilisateur(@PathVariable String identifiant) {
		us.delete(identifiant).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "L'utilisateur "+ identifiant +" n'existe pas"));
	}

}