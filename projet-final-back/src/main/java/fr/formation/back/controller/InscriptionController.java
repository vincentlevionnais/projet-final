package fr.formation.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.back.model.Personne;
import fr.formation.back.model.dto.MessageDTO;
import fr.formation.back.service.PersonneService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/inscription")
public class InscriptionController {
	
	@Autowired
	PersonneService ps;
	
    // @PostMapping("")
    // public String inscription(@RequestBody Utilisateur utilisateur) {
    //     us.inscription(utilisateur);
    //     return "Utilisateur créé";
    // }

    @PostMapping("")
    public MessageDTO inscription(@RequestBody Personne personne) {
        ps.inscription(personne);
        MessageDTO message = new MessageDTO("Personne créée");
        return message;
    }

}
