package fr.formation.back.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.back.model.Filiere;
import fr.formation.back.model.Personne;
import fr.formation.back.model.dto.FiliereDTO;
import fr.formation.back.service.FiliereService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/filiere")
public class FiliereController {

	@Autowired
	FiliereService fs;

	@GetMapping("")
	public List<Filiere> getAll() {return fs.findAll();}

	@GetMapping("/{id}")
	public Filiere getById(@PathVariable Integer id) {
		return fs.getById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
	}

	/*
	 * Il est possible d'affecter des stagiaires a la filière créer directement En
	 * mettant une liste de stagiaire (leur id) dans le body de la requète (détail
	 * dans la méthode create de FiliereService)
	 */
	@PostMapping("")
	public void createFiliere(@RequestBody Filiere f) {
		fs.create(f);
	}

	@PutMapping("")
	public void updateFiliere(@RequestBody Filiere f) {
		fs.update(f);
	}

	@DeleteMapping("/{id}")
	public void deleteFiliere(@PathVariable Integer id) {
		fs.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
	}

	/*
	 * Renvoie la filière avec sa date de début et fin selon les modules associés
	 */
	@GetMapping("/detail/{id}")
	public FiliereDTO getByIdWithDetails(@PathVariable Integer id) {
		return fs.getByIdWithDetail(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
	}

	/*
	 * Affecte la liste de personnes donnée en body de la requète à la filière mis dans l'url
	 */
	@PutMapping("/stagiaires/{id}")
	public void setStagiairesOfFiliere(@RequestBody List<Personne> personnes, @PathVariable Integer id) {
		fs.setStagiairesOfFiliere(personnes, id);
	}

}
