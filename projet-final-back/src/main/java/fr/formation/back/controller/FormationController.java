package fr.formation.back.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.back.model.Formation;
import fr.formation.back.service.FormationService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/formation")
public class FormationController {
	
	@Autowired
	private FormationService fs;
	
	@GetMapping("")
	public List<Formation> findAll() {
		return fs.findAll();
	}

	@GetMapping("/{id}")
	public Formation getById(@PathVariable String id) {
		return fs.getById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La Formation n'existe pas"));
	}

	@PostMapping("")
	public void createFormation(@RequestBody Formation f) {
		fs.create(f);
	}

	@PutMapping("")
	public void updateFormation(@RequestBody Formation f) {
		fs.update(f);
	}

	@DeleteMapping("/{id}")
	public void deleteFormation(@PathVariable String id) {
		fs.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La formation n'existe pas"));
	}

}
