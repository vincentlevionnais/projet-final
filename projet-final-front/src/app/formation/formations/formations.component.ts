import { Component, OnInit } from '@angular/core';
import { Formation } from 'src/app/interfaces/formation';
import { FormationService } from 'src/app/services/formation.service';

@Component({
  selector: 'app-formations',
  templateUrl: './formations.component.html',
  styleUrls: ['./formations.component.css']
})
export class FormationsComponent implements OnInit {
  formation: Formation = {
    libelle: ''
  }
  formations: Formation[] = [];

  constructor(private fs: FormationService) { }

  ngOnInit(): void {

    this.fs.getAll().subscribe((formations: Formation[]) => this.formations = formations);
  }

  onDelete(libelle: string) {
    this.fs.delete(libelle)
  }

  receptionFormation(formation: Formation) {
    this.formations.push(formation)
  }

}
