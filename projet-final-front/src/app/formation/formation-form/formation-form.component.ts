import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Formation } from 'src/app/interfaces/formation';
import { FormationService } from 'src/app/services/formation.service';

@Component({
  selector: 'formation-form',
  templateUrl: './formation-form.component.html',
  styleUrls: ['./formation-form.component.css']
})
export class FormationFormComponent implements OnInit {

  actionText: 'Ajouter' | 'Modifier' = 'Ajouter';
  formation: Formation = {
    libelle: ''
  }
  @Output() OnClick: EventEmitter<void> = new EventEmitter();


  constructor(private route: Router, private fs: FormationService) { }

  ngOnInit(): void {
  }

  handleSubmit() {
    if (this.formation.libelle) {
      console.log('Updating formation...');
      const { libelle } = this.formation;
      const formation: Formation = { libelle };
      this.fs.update(formation)
        .subscribe(() => {
          this.route.navigate(['/formations']);
        })
    } else {
      console.log('Adding formation...');
      this.fs.post(this.formation)
        .subscribe(() => {
          this.route.navigate(['/formations']);
        })
      this.OnClick.emit(console.log("okkk")
      );
    }
  }

}
