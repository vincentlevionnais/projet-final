import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormationComponent } from './formation/formation.component';
import { FormationsComponent } from './formations/formations.component';
import { FormationFormComponent } from './formation-form/formation-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FormationComponent,
    FormationsComponent,
    FormationFormComponent
  ],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule
  ],
  exports: [
    FormationComponent, FormationFormComponent, FormationsComponent
  ]

})
export class FormationModule { }
