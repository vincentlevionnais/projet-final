import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from '../interfaces/utilisateur';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  isUser = false;
  isStagiaire = false;
  isFormateur = false;
  isGestionnaire = false;
  actionText = "Connection";

  utilisateur: Utilisateur = {
    identifiant: '',
    password: '',
    roles: [],
    personne: null
  }

  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  log() {
    if(this.actionText = "Connection"){
      this.logIn();
    } else {this.logOut();}
  }


  logIn(){
    this.actionText = "Déconnection";
    this.router.navigate(['/authentification']);
    this.isGestionnaire = true;

  }

  logOut() {
    this.isUser = false; this.isFormateur = false; this.isStagiaire = false; this.isGestionnaire = false;
    this.actionText = "Connection";
    this.router.navigate(['']);

  }
}



