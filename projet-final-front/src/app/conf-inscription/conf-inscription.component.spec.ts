import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfInscriptionComponent } from './conf-inscription.component';

describe('ConfInscriptionComponent', () => {
  let component: ConfInscriptionComponent;
  let fixture: ComponentFixture<ConfInscriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfInscriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
