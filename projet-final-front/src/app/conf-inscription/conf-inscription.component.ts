import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs';
import { Personne } from '../interfaces/personne';
import { Utilisateur } from '../interfaces/utilisateur';
import { PersonneService } from '../services/personne.service';
import { UtilisateurService } from '../services/utilisateur.service';

@Component({
  selector: 'app-conf-inscription',
  templateUrl: './conf-inscription.component.html',
  styleUrls: ['./conf-inscription.component.css']
})
export class ConfInscriptionComponent implements OnInit {

  utilisateur: Utilisateur | null = null;
  personne: Personne | null = null;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private us: UtilisateurService,
    private ps: PersonneService) { }

  ngOnInit(): void {
    const id: string | null = this.route.snapshot.paramMap.get('id');

      this.ps.getById(Number(id))
        .subscribe((personne: Personne) => {
          this.personne = personne;
        })
    };
  }

  // mypersonne(personne: Personne){
  //   this.personne = personne;

  //   let id = personne.id;


  //   const source$ = this.ps.getById(id);

  //   let pid = this.ps.getById(id);
  //   console.log(pid);
  //   let upid = this.utilisateur?.personne?.id;
  //   console.log(upid);




    // source$.pipe(map((res) => res.))

    // //personne1: Personne = ps;
    // if(id == this.us.getById().pipe(map((data: utilisateur) => data.id).subscribe()))
    //   this.personne = mypersonne;
 // }
//}
