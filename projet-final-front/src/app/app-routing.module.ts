import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthentificationComponent } from './authentification/authentification.component';
import { ConfInscriptionComponent } from './conf-inscription/conf-inscription.component';
import { FiliereFormComponent } from './filiere/filiere-form/filiere-form.component';
import { FiliereComponent } from './filiere/filiere/filiere.component';
import { FilieresComponent } from './filiere/filieres/filieres.component';
import { FormationsComponent } from './formation/formations/formations.component';
import { HomeComponent } from './home/home.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ModuleFormComponent } from './module/module-form/module-form.component';
import { ModuleComponent } from './module/module/module.component';
import { ModulesComponent } from './module/modules/modules.component';
import { PersonneFormComponent } from './personne/personne-form/personne-form.component';
import { PersonneComponent } from './personne/personne/personne.component';
import { PersonnesComponent } from './personne/personnes/personnes.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'authentification', component: AuthentificationComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'conf-inscription', component: ConfInscriptionComponent },

  { path: 'filieres', component: FilieresComponent },
  { path: 'filieres/:id', component: FiliereComponent },
  { path: 'filiere-form', component: FiliereFormComponent },
  { path: 'filiere-form/:id', component: FiliereFormComponent },

  { path: 'modules', component: ModulesComponent },
  { path: 'modules/:id', component: ModuleComponent },
  { path: 'module-form', component: ModuleFormComponent },
  { path: 'module-form/:id', component: ModuleFormComponent },

  { path: 'formations', component: FormationsComponent },


  { path: 'personnes', component: PersonnesComponent },
  { path: 'personnes/:id', component: PersonneComponent },
  { path: 'personne-form', component: PersonneFormComponent },
  { path: 'personne-form/:id', component: PersonneFormComponent },

  { path: 'profile/:id', component: ProfileComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
