import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Formation } from '../interfaces/formation';
import { Personne } from '../interfaces/personne';
import { FormationService } from '../services/formation.service';
import { InscriptionService } from '../services/inscription.service';
import { PersonneService } from '../services/personne.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  @Output()
  OnClick: EventEmitter<Personne> = new EventEmitter();

  formations: Formation[] = [];

  personne: Personne = {
    id: 0,
    nom: '',
    prenom: '',
    mail: '',
    telephone: '',
    dateNaissance: '',
    rue: '',
    codePostal: '',
    ville: '',
    formation: undefined
  }

  actionText: 'Ajouter' | 'Modifier' = 'Ajouter';

  constructor(
    private fs: FormationService,
    private ps: PersonneService,
    private is: InscriptionService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id: string | null = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.actionText = 'Modifier'
      this.ps.getById(Number(id))
        .subscribe((personne: Personne) => {
          this.personne = personne;
        })
    }
    this.fs.getAll().subscribe((formation: Formation[]) => {
      this.formations = formation;
    });
  }

  handleSubmit() {
    console.log('Adding personne...');
    this.is.post(this.personne)
      .subscribe(() => {
        this.router.navigate(['/conf-inscription']);
      })
  }
}
