import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Utilisateur } from '../interfaces/utilisateur';
import { PersonneComponent } from '../personne/personne/personne.component';
import { UtilisateurService } from './utilisateur.service';

const API = API_URL + '/authentification';
@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(private http: HttpClient) { }


  post(utilisateur: Utilisateur)  {
    return this.http.post(API, utilisateur);
  }
  get(utilisateur:Utilisateur){
    return this.http.get(API + '/id')
  }


  // getById(id: number): Observable<Filiere> {
  //   return this.http.get<Filiere>(API + '/' + id);
  // }


}


