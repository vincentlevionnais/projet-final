import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Personne } from '../interfaces/personne';
import { Utilisateur } from '../interfaces/utilisateur';
const API = API_URL + '/utilisateur';
@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(private http: HttpClient) { }

  getById(id: string): Observable<Utilisateur> {
    return this.http.get<Utilisateur>(API + '/' + id);
  }

}
