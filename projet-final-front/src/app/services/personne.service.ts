import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';

import { Personne } from '../interfaces/personne';
const API = API_URL + '/utilisateur';
@Injectable({
  providedIn: 'root'
})
export class PersonneService {


  constructor(private http: HttpClient) { }

  getAll(): Observable<Personne[]> {
    return this.http.get<Personne[]>(API);
  }

  getById(id: number): Observable<Personne> {
    return this.http.get<Personne>(API + '/' + id);
  }

  post(personne: Personne) {
    return this.http.post(API, personne);
  }

  update(personne: Personne) {
    return this.http.put(API, personne);
  }

  delete(id: number) {
    return this.http.delete(API + '/' + id);
  }
}
