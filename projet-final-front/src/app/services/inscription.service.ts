import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Personne } from '../interfaces/personne';

const API = API_URL + '/inscription';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private http: HttpClient) { }

  getById(id: number): Observable<Personne> {
    return this.http.get<Personne>(API + '/' + id);
  }

  post(personne: Personne) {
    return this.http.post(API, personne);
  }
}
