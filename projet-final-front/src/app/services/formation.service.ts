import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { API_URL } from 'src/config/settings';
import { Formation } from '../interfaces/formation';

const API = API_URL + '/formation';

@Injectable({
  providedIn: 'root'
})
export class FormationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Formation[]> {
    return this.http.get<Formation[]>(API)
      .pipe(
        map((data: Formation[]) => data)
      )
  }

  getByLibelle(libelle: string): Observable<Formation> {
    return this.http.get<Formation>(API + '/' + libelle);
  }

  post(formation: Formation) {
    return this.http.post(API, formation);
  }

  update(formation: Formation) {
    return this.http.put(API, formation);
  }

  delete(libelle: string) {
    return this.http.delete(API + '/' + libelle);
  }
}
