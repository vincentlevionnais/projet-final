import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonneComponent } from './personne/personne.component';
import { PersonnesComponent } from './personnes/personnes.component';
import { PersonneFormComponent } from './personne-form/personne-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [
    PersonneComponent,
    PersonnesComponent,
    PersonneFormComponent
  ],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, RouterModule
  ],
  exports: [
    PersonneComponent, PersonneFormComponent, PersonnesComponent
  ]
})
export class PersonneModule { }
