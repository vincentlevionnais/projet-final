import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError } from 'rxjs';
import { Personne } from 'src/app/interfaces/personne';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-personnes',
  templateUrl: './personnes.component.html',
  styleUrls: ['./personnes.component.css']
})
export class PersonnesComponent implements OnInit {
  personnes: Personne[] = [];

  constructor(
    private ps: PersonneService,
    private router: Router
  ) { }

  message: string = "";

  private clearMessage() {
    setTimeout(() => this.message = "", 5000);
  }

  ngOnInit(): void {
    this.ps.getAll()
      .subscribe((personnes: Personne[]) => {
        this.personnes = personnes
      });
  }

  onView(id?: number) {
    this.router.navigate(['/personnes', id])
  }

  onEdit(id?: number) {
    this.router.navigate(['/personne-form', id])
  }

  onDelete(id?: number) {
    if (id) {
      this.ps.delete(id)
        .pipe(
          catchError((err: HttpErrorResponse) => {
            console.log(err.message);
            this.message = `Suppression impossible`;
            this.clearMessage();
            return [];
          })
        )
        .subscribe(() => {
          this.personnes = this.personnes.filter(personne => personne.id !== id);
        })
    }
  }
}