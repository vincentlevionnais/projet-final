import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Personne } from 'src/app/interfaces/personne';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

  personne: Personne = {
    id: 0,
    nom: '',
    prenom: '',
    mail: '',
    telephone: '',
    dateNaissance: '',
    rue: '',
    codePostal: '',
    ville: ''
  };
  constructor(
    private route: ActivatedRoute,
    private ps: PersonneService
  ) { }


  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.ps.getById(Number(id))
      .subscribe((personne: Personne) => {
        this.personne = personne;
      })
  }
}