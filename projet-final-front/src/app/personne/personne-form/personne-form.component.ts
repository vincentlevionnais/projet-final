import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Formation } from 'src/app/interfaces/formation';
import { Personne } from 'src/app/interfaces/personne';
import { FormationService } from 'src/app/services/formation.service';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-personne-form',
  templateUrl: './personne-form.component.html',
  styleUrls: ['./personne-form.component.css']
})
export class PersonneFormComponent implements OnInit {

  formations: Formation[] = [];

  personne: Personne = {
    id: 0,
    nom: '',
    prenom: '',
    mail: '',
    telephone: '',
    dateNaissance: '',
    rue: '',
    codePostal: '',
    ville: '',
    formation: undefined
  }

  actionText: 'Ajouter' | 'Modifier' = 'Ajouter';

  constructor(
    private fs: FormationService,
    private ps: PersonneService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id: string | null = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.actionText = 'Modifier'
      this.ps.getById(Number(id))
        .subscribe((personne: Personne) => {
          this.personne = personne;
        })
    }
    this.fs.getAll().subscribe((formation: Formation[]) => {
      this.formations = formation;
    });
  }

  handleSubmit() {
    if (this.personne.id) {
      console.log('Updating personne...');
      const { id, nom, prenom, mail, telephone, dateNaissance, rue, codePostal, ville, formation } = this.personne;
      const personne: Personne = { id, nom, prenom, mail, telephone, dateNaissance, rue, codePostal, ville, formation }
      this.ps.update(personne)
        .subscribe(() => {
          this.router.navigate(['/personnes']);
        })
    } else {
      console.log('Adding personne...');
      this.ps.post(this.personne)
        .subscribe(() => {
          this.router.navigate(['/personnes']);
        })
    }
  }

}
