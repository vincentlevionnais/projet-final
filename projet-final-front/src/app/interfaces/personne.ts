import { Formation } from "./formation";

export interface Personne {
    id: number;
    nom: string;
    prenom: string;
    mail: string;
    telephone: string;
    dateNaissance: string;
    rue: string;
    codePostal: string;
    ville: string;
    formation?: Formation;
}
