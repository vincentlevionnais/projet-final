import { Personne } from "./personne";
import { Role } from "./role";

export interface Utilisateur {
  identifiant: string;
  password: string;
  roles: Role[];
  personne: Personne | null;
}
