import { Component, OnInit } from '@angular/core';
import { Personne } from '../interfaces/personne';
import { PersonneService } from 'src/app/services/personne.service';
import { ActivatedRoute } from '@angular/router';
import { UtilisateurService } from '../services/utilisateur.service';
import { Utilisateur } from '../interfaces/utilisateur';
import { map } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  urlParam : string | null = '';

  profile : Personne | null =  {
    id: 0,
    nom: '',
    prenom: '',
    mail: '',
    telephone: '',
    dateNaissance: '',
    rue: '',
    codePostal: '',
    ville: ''
  }

  constructor(
    private ps: PersonneService,
    private route: ActivatedRoute,
    private us: UtilisateurService
  ) {

  }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('id');

    console.log(this.urlParam);

    const source$ = this.us.getById(String (this.urlParam));
    source$.pipe(map((res) => res.personne)).subscribe(personne => {this.profile = personne});

  }
}
