import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from '../interfaces/utilisateur';
import { Personne } from '../interfaces/personne';
import { AuthentificationService } from '../services/authentification.service';
import { PersonneService } from '../services/personne.service';
import { UtilisateurService } from '../services/utilisateur.service';
import { map  } from 'rxjs';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css'],
})
export class AuthentificationComponent implements OnInit {

  personne: Personne = {
    id: 0,
    nom: '',
    prenom: '',
    mail: '',
    telephone: '',
    dateNaissance: '',
    rue: '',
    codePostal: '',
    ville: '',
    formation: undefined
  }

   utilisateur: Utilisateur = {
    identifiant: '',
    password: '',
    roles: [{role:'ROLE_GESTIONNAIRE'}],
   personne: null
  };

  constructor(private router: Router, private as: AuthentificationService, private us: UtilisateurService,private ps: PersonneService) {}

  ngOnInit(): void {}

  handleSubmit() {
    console.log('login...');

    this.us.getById(this.utilisateur.identifiant)
    .pipe(
      map(user=>user.identifiant)
    )
    .subscribe((id)=>{
      this.router.navigate(['/profile/'+id]);
    });
  }
}
