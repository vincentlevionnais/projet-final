import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FilieresComponent } from './filiere/filieres/filieres.component';
import { FiliereFormComponent } from './filiere/filiere-form/filiere-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FiliereComponent } from './filiere/filiere/filiere.component';
import { HomeComponent } from './home/home.component';
import { ModuleFormComponent } from './module/module-form/module-form.component';
import { ModulesComponent } from './module/modules/modules.component';
import { ModuleComponent } from './module/module/module.component';
import { PersonneModule } from './personne/personne.module';
import { FormationModule } from './formation/formation.module';
import { ProfileComponent } from './profile/profile.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConfInscriptionComponent } from './conf-inscription/conf-inscription.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FilieresComponent,
    FiliereFormComponent,
    FiliereComponent,
    HomeComponent,
    ModuleFormComponent,
    ModulesComponent,
    ModuleComponent,
    ProfileComponent,
    AuthentificationComponent,
    InscriptionComponent,
    ConfInscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PersonneModule,
    FormationModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
